﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Shooter : MonoBehaviour {
    public GameObject obj;

    public LineRenderer line;

    public int Force;
    public SpringJoint template;

    // Use this for initialization
    void Start() {
        line = GetComponentInChildren<LineRenderer>();
    }

    private SpringJoint joint;

    public void Shoot() {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        var mainTransform = Camera.main.transform;
        if (Physics.Raycast(mainTransform.position, mainTransform.TransformDirection(Vector3.forward), out hit,
            Mathf.Infinity)) {
            Debug.DrawRay(mainTransform.position, mainTransform.TransformDirection(Vector3.forward) * hit.distance,
                Color.yellow);
            Debug.Log("Did Hit " + hit.transform.name);
            line.SetPosition(1,hit.point);
//			obj.transform.position = hit.point;
            gameObject.GetComponent<RigidbodyFirstPersonController>().Jump();
            Force = 10000;
            var body = gameObject.GetComponent<Rigidbody>();
//            body
//                .AddForce(mainTransform.TransformDirection(Vector3.forward)  * Force);

            if (joint != null) {
                Destroy(joint.gameObject);
            }
            
            joint = Instantiate(template, hit.point, Quaternion.identity);

//            joint.autoConfigureConnectedAnchor = false;
//            joint.anchor = hit.point;
            joint.connectedBody = body;
            joint.connectedAnchor = new Vector3(0, 0, 0);
            Debug.Log(body);
            joint.damper = template.damper;
            joint.spring = template.spring;
            joint.tolerance = template.tolerance;
            joint.breakForce = template.breakForce;
            joint.breakTorque = template.breakTorque;
            joint.connectedMassScale = template.connectedMassScale;
        }
        else {
            Debug.DrawRay(mainTransform.position, mainTransform.TransformDirection(Vector3.forward) * 1000,
                Color.white);
            Debug.Log("Did not Hit");
        }
        
    
    }

// Update is called once per frame
    void Update() {
       
        if (joint != null) {
            line.SetPosition(0, joint.anchor);
            line.SetPosition(1, joint.connectedAnchor);
        }
        
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            Shoot();
        }

        if (Input.GetKeyUp(KeyCode.Mouse0)) {
            if (joint != null) {
                Destroy(joint.gameObject);
            }
        }
        
    }
}